﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumer
{
    public class GivingToCustomerConsumer : IConsumer<GivePromoCodeToCustomer>
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly ILogger<GivingToCustomerConsumer> _logger;

        public GivingToCustomerConsumer(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository, ILogger<GivingToCustomerConsumer> logger)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _logger = logger;
        }


        public async Task Consume(ConsumeContext<GivePromoCodeToCustomer> context)
        {
            var preference = await _preferencesRepository.GetByIdAsync(context.Message.PreferenceId);

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(context.Message, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            _logger.LogInformation("Получен промокод: {context.Message}", context.Message);
        }
    }
}
