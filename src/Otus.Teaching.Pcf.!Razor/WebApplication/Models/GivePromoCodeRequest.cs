﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;

namespace WebApplication.Models
{
    public class GivePromoCodeRequest : BaseEntity
    {
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }

        public Guid PromoCodeId { get; set; }
        
        public string PromoCode { get; set; }

        public Guid PreferenceId { get; set; }

        public string BeginDate { get; set; }

        public string EndDate { get; set; }
        public SelectList Prefer { get; internal set; }
        public SelectList Partner { get; internal set; }
    }
}