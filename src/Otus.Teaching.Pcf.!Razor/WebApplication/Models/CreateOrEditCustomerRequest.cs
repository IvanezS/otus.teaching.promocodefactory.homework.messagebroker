﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;

namespace WebApplication.Models
{
    /// <example>
    /// {
    ///    "firstName": "Иван",
    ///    "lastName": "Васильев",
    ///     "email": "ivan_vasiliev@somemail.ru",
    ///        "preferenceIds": [
    ///            "c4bda62e-fc74-4256-a956-4760b3858cbd"
    ///        ]
    /// }
    /// </example>>
    public class CreateOrEditCustomerRequest : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public List<Guid> PreferenceIds { get; set; } = new List<Guid>();
        public MultiSelectList Prefer { get; set; }

        //public IEnumerable<Preference> PreferenceIds { get; set; } = new List<Preference>();
    }
}