﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace WebApplication.Models
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    
    }
}
