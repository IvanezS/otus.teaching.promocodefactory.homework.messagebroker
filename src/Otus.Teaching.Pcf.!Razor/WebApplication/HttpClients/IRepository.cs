﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace WebApplication.Repositories
{
    public interface IRepo<T>
        where T: BaseEntity
    {
        public Task<IEnumerable<T>> GetAllAsync();

        public Task<T> GetByIdAsync(Guid id);

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

        public Task AddAsync(T entity);

        public Task UpdateAsync(T entity);

        public Task DeleteAsync(T entity);
    }
}