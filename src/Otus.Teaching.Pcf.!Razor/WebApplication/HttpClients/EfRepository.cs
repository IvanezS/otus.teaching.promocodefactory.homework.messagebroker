﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using WebApplication.Models;
using WebApplication.Repositories;


namespace WebApplication.HttpClients
{
    public class EfRepository<T>
        : IRepo<T>
        where T : BaseEntity
    {
        private readonly HttpClient _client;

        public EfRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var clientReq = $"{_client.BaseAddress}api/v1/{typeof(T).Name}s";
            var entities = await _client.GetAsync(clientReq);
            if (typeof(T).Name == "Partner")
            {
                var format = "dd.MM.yyyy HH:mm:ss"; // your datetime format 16.06.2020 12:00:00
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };
                return JsonConvert.DeserializeObject<IEnumerable<T>>(await entities.Content.ReadAsStringAsync(), dateTimeConverter);
            }
            return JsonConvert.DeserializeObject<IEnumerable<T>>(await entities.Content.ReadAsStringAsync()); 
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            string ent;
            if (typeof(T).Name == "CreateOrEditCustomerRequest" || typeof(T).Name == "CustomerResponse")
            {
                ent = "Customer";
            }
            else if (typeof(T).Name == "GivePromoCodeRequest")
            {
                ent = "Promocode";
            }
            else
            {
                ent = typeof(T).Name;
            }
            var clientReq = $"{_client.BaseAddress}api/v1/{ent}s/{id}";
            var entities = await _client.GetAsync(clientReq);

            return JsonConvert.DeserializeObject<T>(await entities.Content.ReadAsStringAsync());
        }

        //public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        //{
        //    var entities = await _dataContext.Set<T>().Where(x => ids.Contains(x.Id)).ToListAsync();
        //    return entities;
        //}

        //public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        //{
        //    return await _dataContext.Set<T>().FirstOrDefaultAsync(predicate);
        //}

        //public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        //{
        //    return await _dataContext.Set<T>().Where(predicate).ToListAsync();
        //}

        public async Task AddAsync(T entity)
        {
            string ent;
            if (typeof(T).Name == "CreateOrEditCustomerRequest" || typeof(T).Name == "CustomerResponse")
            {
                ent = "Customer";
            }
            else if (typeof(T).Name == "GivePromoCodeRequest")
            {
                ent = "Promocode";
            }
            else 
            {
                ent = typeof(T).Name;
            }
            await _client.PostAsJsonAsync($"{_client.BaseAddress}api/v1/{ent}s", entity);
        }

        public async Task UpdateAsync(T entity)
        {
            string ent;
            if (typeof(T).Name == "CreateOrEditCustomerRequest")
            {
                ent = "Customer";
            }
            else if (typeof(T).Name == "GivePromoCodeRequest")
            {
                ent = "Promocode";
            }
            else
            {
                ent = typeof(T).Name;
            }
            await _client.PutAsJsonAsync($"{_client.BaseAddress}api/v1/{ent}s/{entity.Id}", entity);
        }

        public async Task DeleteAsync(T entity)
        {
            await _client.DeleteAsync($"{_client.BaseAddress}api/v1/{typeof(T).Name}s/{entity.Id}");
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
