using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.HttpClients;
using WebApplication.Models;
using WebApplication.Repositories;

namespace WebApplication
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            //����������� ��������������� ������� GivingToCustomer
            services.AddHttpClient<HttpClients.EfRepository<Preference>> 
            (client =>
                {
                    client.BaseAddress = new Uri("http://localhost:8093");
                }
            );
            services.AddHttpClient<HttpClients.EfRepository<Customer>>
            (client =>
                {
                    client.BaseAddress = new Uri("http://localhost:8093");
                }
            );
            services.AddHttpClient<HttpClients.EfRepository<CreateOrEditCustomerRequest>>
            (client =>
            {
                client.BaseAddress = new Uri("http://localhost:8093");
            }
            );
            services.AddHttpClient<HttpClients.EfRepository<PromoCode>>
            (client =>
                {
                    client.BaseAddress = new Uri("http://localhost:8093");
                }
            );
            services.AddHttpClient<HttpClients.EfRepository<Partner>>
            (client =>
                {
                    client.BaseAddress = new Uri("http://localhost:8092");
                }
            );
            services.AddHttpClient<HttpClients.EfRepository<GivePromoCodeRequest>>
            (client =>
                {
                    client.BaseAddress = new Uri("http://localhost:8093");
                }
            );
            services.AddHttpClient<HttpClients.EfRepository<CustomerResponse>>
            (client =>
            {
                client.BaseAddress = new Uri("http://localhost:8093");
            }
            );

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
