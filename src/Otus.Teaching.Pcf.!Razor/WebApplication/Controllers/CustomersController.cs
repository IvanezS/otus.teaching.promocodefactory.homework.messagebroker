﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication.HttpClients;
using WebApplication.Models;
using WebApplication.Mappers;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    //[ApiController]
    //[Route("api/v1/[controller]")]
    public class CustomersController
        : Controller
    {
        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<CustomerResponse> _customerResp;
        private readonly EfRepository<CreateOrEditCustomerRequest> _customerCreateOrEdit;
        private readonly EfRepository<Preference> _preferenceRepository;
        

        public CustomersController( EfRepository<Customer> customerRepository,
                                    EfRepository<CreateOrEditCustomerRequest> customerCreateOrEdit,
                                    EfRepository<CustomerResponse> customerResp,
                                    EfRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _customerResp = customerResp;
            _customerCreateOrEdit = customerCreateOrEdit;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> Index()
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return View(response);
        }

        public async Task<ActionResult<CreateOrEditCustomerRequest>> Create()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var response = new CreateOrEditCustomerRequest();

            var pref = preferences.Select(c => new
            {
                PreferenceIds = c.Id,
                PreferenceName = c.Name
            });

            response.Prefer = new MultiSelectList(pref, "PreferenceIds", "PreferenceName");

            return View(response);
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult<CustomerResponse>> Create([Bind("LastName,FirstName,Email,PreferenceIds,Prefer")] CreateOrEditCustomerRequest request)
        {
            if (ModelState.IsValid)
            {
                await _customerCreateOrEdit.AddAsync(request);

                return RedirectToAction("Index");
            }

            return View("Create");
        }


        public async Task<ActionResult<CreateOrEditCustomerRequest>> Edit(Guid id)
        {

            var customer = await _customerResp.GetByIdAsync(id);
            var response = new CreateOrEditCustomerRequest()
            {
                FirstName = customer.FirstName,
                Email = customer.Email,
                Id = customer.Id,
                LastName = customer.LastName,
                

            };
            
            var preferences = await _preferenceRepository.GetAllAsync();
            if (preferences != null)
            {


                var pref = preferences.Select(c => new
                {
                    PreferenceIds = c.Id,
                    PreferenceName = c.Name
                });

   
                foreach (var pr in customer.Preferences) response.PreferenceIds.Add(pr.Id);

                response.Prefer = new MultiSelectList(pref, "PreferenceIds", "PreferenceName", response.PreferenceIds);
            }

            return View(response);
        }





        /// <summary>
        /// Обновить клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,LastName,FirstName,Email,PreferenceIds,Prefer")] CreateOrEditCustomerRequest request)
        {
            if (ModelState.IsValid)
            {
                await _customerCreateOrEdit.UpdateAsync(request);

                return RedirectToAction("Index");
            }

            return View("Edit");
        }



        public async Task<ActionResult<Customer>> DeleteAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return View(customer);
        }



        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id">Id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete([Bind("Id")] Guid id)
        {
            if (ModelState.IsValid)
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                if (customer == null) return NotFound();

                await _customerRepository.DeleteAsync(customer);
                return RedirectToAction("Index");
            }

            return View("Delete");
        }
    }
    



}
