﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using WebApplication.Models;
using WebApplication.HttpClients;
using Preference = WebApplication.Models.Preference;


namespace WebApplication.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    //[ApiController]
    //[Route("api/v1/[controller]")]
    public class PreferencesController
        : Controller
    {
        private readonly EfRepository<Preference> _preferencesRepository;

        public PreferencesController(EfRepository<Preference> preferencesRepository)
        {
            _preferencesRepository = preferencesRepository;
        }
        
        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> Index()//GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new Preference()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return View(response);// Ok(response);
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create( [Bind("Name")] Preference items)
        {
 
            if (ModelState.IsValid)
            {
                await _preferencesRepository.AddAsync(items);

                return RedirectToAction("Index");
            }

            return View(items);
        }





    }
}