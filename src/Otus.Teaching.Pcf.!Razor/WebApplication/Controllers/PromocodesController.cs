﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApplication.HttpClients;
using WebApplication.Mappers;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    //[ApiController]
    //[Route("api/v1/[controller]")]
    public class PromocodesController
        : Controller
    {
        private readonly EfRepository<GivePromoCodeRequest> _promoGivRepository;
        private readonly EfRepository<PromoCode> _promoCodesRepository;
        private readonly EfRepository<Preference> _preferencesRepository;
        private readonly EfRepository<Customer> _customersRepository;
        private readonly EfRepository<Partner> _partnersRepository;

        public PromocodesController(EfRepository<PromoCode> promoCodesRepository,
                                    EfRepository<Preference> preferencesRepository,
                                    EfRepository<Partner> partnersRepository,
                                    EfRepository<Customer> customersRepository, 
                                    EfRepository<GivePromoCodeRequest> promoGivRepository)
        {
            _promoGivRepository = promoGivRepository;
            _promoCodesRepository = promoCodesRepository;
            _partnersRepository = partnersRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> Index()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();

            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return View(response);
        }


        public async Task<ActionResult<GivePromoCodeRequest>> CreateAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();
            var response = new GivePromoCodeRequest();

            var partners = await _partnersRepository.GetAllAsync();

            var pref = preferences.Select(c => new
            {
                PreferenceId = c.Id,
                PreferenceName = c.Name
            });

            var partn = partners.Select(c => new
            {
                PartnerId = c.Id,
                PartnerName = c.Name
            });

            response.Prefer = new SelectList(pref, "PreferenceId", "PreferenceName");
            response.Partner = new SelectList(partn, "PartnerId", "PartnerName");

            return View(response);
        }


        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateAsync([Bind("ServiceInfo,PartnerId,PromoCode,PreferenceId,BeginDate,EndDate,Prefer")] GivePromoCodeRequest request)
        {

            if (ModelState.IsValid)
            {
                await _promoGivRepository.AddAsync(request);

                return RedirectToAction("Index");
            }

            return View("Create");



        }
    }
}