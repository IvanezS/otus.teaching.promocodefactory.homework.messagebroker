﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        readonly IPublishEndpoint _publishEndpoint;


        public interface IUpdateAdministration
        {
            public Guid PartnerManagerId { get; set; }
        }

        public AdministrationGateway(IPublishEndpoint publishEndpoint)
        {
            _publishEndpoint = publishEndpoint;
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _publishEndpoint.Publish<IUpdateAdministration>(new
            {
                PartnerManagerId = partnerManagerId
            });
        }
    }
}