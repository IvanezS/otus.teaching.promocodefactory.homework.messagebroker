﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumer
{
    public class AdministrationConsumer : IConsumer<IUpdateAdministration>
    {

        private readonly IRepository<Employee> _employeeRepository;
        private readonly ILogger<AdministrationConsumer> _logger;

        public AdministrationConsumer(IRepository<Employee> employeeRepository, ILogger<AdministrationConsumer> logger)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<IUpdateAdministration> context)
        {
            var employee = await _employeeRepository.GetByIdAsync(context.Message.PartnerManagerId);

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            _logger.LogInformation("Получен PartnerManagerId: {PartnerManagerId}", context.Message.PartnerManagerId);
        }



    }
}
