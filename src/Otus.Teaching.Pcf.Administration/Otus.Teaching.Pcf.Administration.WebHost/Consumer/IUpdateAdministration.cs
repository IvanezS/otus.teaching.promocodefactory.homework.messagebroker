﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumer
{
    public interface IUpdateAdministration
    {
        public Guid PartnerManagerId { get; set; }
    }
}
